import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import ScheduleIcon from "@material-ui/icons/Schedule";
import WbSunnyIcon from "@material-ui/icons/WbSunny";
import CalendarTodayIcon from "@material-ui/icons/CalendarToday";
import CloudIcon from "@material-ui/icons/Cloud";
import WifiIcon from "@material-ui/icons/Wifi";
import MenuBookIcon from "@material-ui/icons/MenuBook";
import MusicVideoIcon from "@material-ui/icons/MusicVideo";
import DesktopWindowsIcon from "@material-ui/icons/DesktopWindows";
import Brightness6Icon from "@material-ui/icons/Brightness6";
import MessageIcon from "@material-ui/icons/Message";
import { CardActionArea, CardContent, Typography } from "@material-ui/core";
import BrightnessSlider from "../componets/BrightnessSlider";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function NestedGrid() {
  const classes = useStyles();
  const [showBrightness, setShowBrightness] = React.useState(false);

  function FormRow() {
    return (
      <React.Fragment>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get("https://raspberrypi:3002/currentweather/toggle")
              }
            >
              <WbSunnyIcon fontSize="large" />
              <CardContent>
                <Typography>Current Weather</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get("https://raspberrypi:3002/currentweather/toggle")
              }
            >
              <CloudIcon fontSize="large" />
              <CardContent>
                <Typography>Weather Forecast</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() => axios.get("https://raspberrypi:3002/clock/toggle")}
            >
              <ScheduleIcon fontSize="large" />
              <CardContent>
                <Typography>Clock</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() => axios.get("https://raspberrypi:3002/clock/toggle")}
            >
              <CalendarTodayIcon fontSize="large" />
              <CardContent>
                <Typography>Calendar</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get("https://raspberrypi:3002/MMM-network-signal/toggle")
              }
            >
              <WifiIcon fontSize="large" />
              <CardContent>
                <Typography>Wifi Signal</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get("https://raspberrypi:3002/newsfeed/toggle")
              }
            >
              <MenuBookIcon fontSize="large" />
              <CardContent>
                <Typography>News Feed</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get(
                  "https://raspberrypi:3002/MMM-NowPlayingOnSpotify/toggle"
                )
              }
            >
              <MusicVideoIcon fontSize="large" />
              <CardContent>
                <Typography>Spotify</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get("https://raspberrypi:3002/monitor/toggle")
              }
            >
              <DesktopWindowsIcon fontSize="large" />
              <CardContent>
                <Typography>Screen off/on</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() => setShowBrightness(true)}
            >
              <Brightness6Icon fontSize="large" />
              <CardContent>
                <Typography>Brightness</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
        <Grid item xs={6}>
          <CardActionArea>
            <Card
              className={classes.paper}
              onClick={() =>
                axios.get("https://raspberrypi:3002/compliments/toggle")
              }
            >
              <MessageIcon fontSize="large" />
              <CardContent>
                <Typography>Messages</Typography>
              </CardContent>
            </Card>
          </CardActionArea>
        </Grid>
      </React.Fragment>
    );
  }

  return (
    <Container>
      <BrightnessSlider
        showBrightness={showBrightness}
        closeBrightnessSlider={() => setShowBrightness(false)}
      />
      <Grid container spacing={1}>
        <Grid container item xs={12} spacing={3}>
          <FormRow />
        </Grid>
      </Grid>
    </Container>
  );
}
