import React, { useState } from "react";
import {
  Button,
  Container,
  Grid,
  Typography,
  Select,
  MenuItem,
  InputLabel,
  Hidden,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { TextField } from "@material-ui/core";
import data from "../locationsData/citiesFilter.list.json";
import axios from "axios";
import { addCreds } from "../api/api";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: 10,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

export default function SetupPage() {
  const classes = useStyles();
  const [weatherId, setWeatherId] = React.useState();
  const [longitude, setLongitude] = React.useState();
  const [latitude, setLatitude] = React.useState();
  const [findCity, setFindCity] = React.useState(true);
  const [locationName, setLocationName] = React.useState();
  const [currentTemp, setCurrentTemp] = React.useState();
  const [publicHolidayCalander, setPublicHolidayCalander] = React.useState();
  const [spotifyClientId, setSpotifyClientId] = React.useState();
  const [spotifyClientSecret, setSpotifyClientSecret] = React.useState();
  const [spotAgree, setSpotAgree] = React.useState(false);

  const testWeather = async () => {
    if (findCity) {
      if (!longitude || !latitude || !weatherId) {
        alert("Please make sure all details are filled");
        return;
      }
      let url = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${weatherId}&units=metric`;
      const res = await axios.get(url).catch(() => {
        return { status: 401 };
      });

      if (res.status === 200) {
        setLocationName(res.data.name + ", " + res.data.sys.country);
        setCurrentTemp(Number(res.data.main.temp).toFixed(1));
      } else {
        alert(
          "Something went wrong during the test, please make sure all details are correctly entered"
        );
      }
    } else {
      if (!locationName || !weatherId) {
        alert("Please make sure all details are filled, 2");
        return;
      }
      let url = `https://api.openweathermap.org/data/2.5/weather?q=${locationName}&appid=${weatherId}&units=metric`;
      const res = await axios.get(url).catch(() => {
        return { status: 401 };
      });

      if (res.status === 200) {
        setLocationName(res.data.name + ", " + res.data.sys.country);
        setCurrentTemp(Number(res.data.main.temp).toFixed(1));
      } else {
        alert(
          "Something went wrong during the test, please make sure all details are correctly entered"
        );
      }
    }
  };

  const getLocation = async () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        setLatitude(Number(position.coords.latitude).toFixed(3));
        setLongitude(Number(position.coords.longitude).toFixed(3));
      });
    }
  };

  return (
    <Container>
      <h1>Mirror Setup</h1>
      <Typography>
        To get the most out of your mirror follow the steps below for each
        component
      </Typography>
      <div className={classes.root}>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>Weather</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              This module displays the current weather, including the windspeed,
              the sunset or sunrise time, the temperature, weather forecast for
              the coming week and an icon to display the current conditions.
              <br />
              <br />
              <Typography variant="h5">Step 1: Create Account</Typography>
              Please create an account at{" "}
              <a
                target="_blank"
                href="https://openweathermap.org/"
                style={{ color: "lightblue" }}
              >
                {" "}
                openweathermap.org
              </a>
              . Note: the free account will be all that is needed
              <br />
              <br />
              <Typography variant="h5">Step 2: Create key</Typography>
              Please create a API key{" "}
              <a
                target="_blank"
                href="https://home.openweathermap.org/api_keys"
                style={{ color: "lightblue" }}
              >
                here
              </a>{" "}
              and enter the key value below.
              <Container style={{ marginTop: 20 }}>
                <TextField
                  required
                  id="standard-required"
                  label="Token"
                  type="password"
                  variant="outlined"
                  onChange={(e) => setWeatherId(e.target.value)}
                />
              </Container>
              <br />
              <Typography variant="h5">Step 3: Select Location</Typography>
              <Typography>
                Click the get location button to automatically use your current
                coordinates to find your location for the weather. To try and
                manually find your location use the manually search cities
                button.
                <br />
                <b>Note:</b> Not all towns and locations are supported, if you
                do not find your exact town or location please pick your closest
                one.
              </Typography>
              <br />
              <Grid container justifyContent="space-between">
                <Grid item xs={4}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={getLocation}
                  >
                    Get Location
                  </Button>
                </Grid>
                <Grid item xs={4}>
                  <Button
                    variant="contained"
                    color="default"
                    onClick={() => setFindCity(false)}
                    style={{ paddingLeft: 10 }}
                  >
                    Manually Search Cities
                  </Button>
                </Grid>
              </Grid>
              <Typography>
                Longitude: {longitude}
                <br />
                Latitude: {latitude}
              </Typography>
              <Autocomplete
                hidden={findCity}
                id="city-name"
                options={data.map(
                  (option) => option.name + ", " + option.country
                )}
                autoSelect
                freeSolo
                renderInput={(params) => (
                  <TextField
                    onChange={(e) => {
                      setLocationName(e.target.value);
                    }}
                    onSelect={(e) => {
                      setLocationName(e.target.value);
                    }}
                    {...params}
                    label="City Name"
                    margin="normal"
                    variant="outlined"
                  />
                )}
              />
              <br />
              <Typography variant="h5">Step 5: Test & Save</Typography>
              <Grid container>
                <Grid item xs={6}>
                  <Button variant="contained" onClick={() => testWeather()}>
                    Test
                  </Button>
                  <Typography>
                    Location Name: <b>{locationName}</b>
                    <br />
                    Current temperature: <b>{currentTemp}°C</b>
                  </Typography>
                </Grid>

                <Grid item xs={6}>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      console.log(findCity);
                      if (findCity) {
                        if (!weatherId || !latitude || !longitude) {
                          alert(
                            "Please make sure all forms are correctly filled"
                          );
                          return;
                        }
                        addCreds({
                          creds: [
                            {
                              cred_num: 1,
                              cred_value: weatherId,
                            },
                            {
                              cred_num: 2,
                              cred_value: latitude,
                            },
                            {
                              cred_num: 3,
                              cred_value: longitude,
                            },
                            {
                              cred_num: 4,
                              cred_value: locationName,
                            },
                          ],
                        });
                      } else {
                        if (!weatherId || !locationName) {
                          alert(
                            "Please make sure all forms are correctly filled"
                          );
                          return;
                        }
                        addCreds({
                          creds: [
                            {
                              cred_num: 1,
                              cred_value: weatherId,
                            },
                            {
                              cred_num: 2,
                              cred_value: "  ",
                            },
                            {
                              cred_num: 3,
                              cred_value: "  ",
                            },
                            {
                              cred_num: 4,
                              cred_value: locationName,
                            },
                          ],
                        });
                      }
                    }}
                  >
                    Save
                  </Button>
                </Grid>
              </Grid>
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Calander</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              This module displays events from a public .ical calendar. It can
              combine multiple calendars.
              <br />
              <br />
              <Typography variant="h5">
                Select Public Holidays Calander
              </Typography>
              <br />
              <InputLabel htmlFor="age-native-simple">Country</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={publicHolidayCalander}
                onChange={(e) => {
                  setPublicHolidayCalander(e.target.value);
                }}
                style={{ width: "50%" }}
              >
                <MenuItem value={"UK"}>United Kingdom</MenuItem>
                <MenuItem value={"IRE"}>Ireland</MenuItem>
              </Select>
              <br />
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: 10 }}
                onClick={() => {
                  if (!publicHolidayCalander) {
                    alert("Please select a calander");
                    return;
                  }
                  addCreds({
                    creds: [
                      {
                        cred_num: 5,
                        cred_value: publicHolidayCalander,
                      },
                    ],
                  });
                }}
              >
                Save
              </Button>
              <br />
              <Typography variant="h5">
                <b>Optional:</b> Add Google Calander
              </Typography>
              <br />
              <Typography>
                To get the ical address of your google calander head to{" "}
                <a
                  target="_blank"
                  href="https://calendar.google.com/calendar/"
                  style={{ color: "lightblue" }}
                >
                  {" "}
                  google.com.calendar
                </a>
                .
                <br />
                From here head to the My calendars section and click into{" "}
                <b>settings and sharing</b>.
                <br />
                <b>
                  Note: Make sure the calander is made public. By ensuring Make
                  available to public is activated.
                </b>
                <br />
                Find the <b>Public address in ical format</b> section and copy
                the address and insert below.
              </Typography>
              <TextField
                id="outlined-basic"
                label="Google iCal address"
                variant="outlined"
                fullWidth
              />
              <Typography>
                Give your calander a name to identify it with.
              </Typography>
              <TextField
                id="outlined-basic"
                label="Calander Name"
                variant="outlined"
                fullWidth
              />
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: 10 }}
              >
                Save
              </Button>
              <br />
              <br />
              <Typography variant="h5">
                <b>Optional:</b> Add Outlook Calander
              </Typography>
              <br />
              <Typography>
                To get the ical address of your Outlook calander head to{" "}
                <a
                  target="_blank"
                  href="https://outlook.live.com/calendar/"
                  style={{ color: "lightblue" }}
                >
                  {" "}
                  outlook.live.com/calendar
                </a>
                .
                <br />
                From here click onto the settings icon, the gear icon, and click{" "}
                <b>View all Outlook settings</b>, at the bottom.
                <br />
                Proceed to Calendar &gt; shared Calendars
                <br />
                From here select the calander you want to include in the mirror
                from the publish a calendar section. And choose the permissions
                you want to include.
                <br />
                Publish the calander and copy and paste the ICS address below.
              </Typography>
              <TextField
                id="outlined-basic"
                label="Outlook iCal address"
                variant="outlined"
                fullWidth
              />
              <Typography>
                Give your calander a name to identify it with.
              </Typography>
              <TextField
                id="outlined-basic"
                label="Calander Name"
                variant="outlined"
                fullWidth
              />
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: 10 }}
              >
                Save
              </Button>
              <Typography variant="h5">
                <b>Optional:</b> Add Apple Calander
              </Typography>
              <br />
              <Typography>
                To get the ical address of your Apple calander head to{" "}
                <a
                  target="_blank"
                  href="https://www.icloud.com/calendar/"
                  style={{ color: "lightblue" }}
                >
                  {" "}
                  icloud.com/calendar
                </a>
                .
                <br />
                Click on the grayed out "wireless" icon.
                <br />
                <b>Make sure the calendar is made public</b>
                <br />
                Copy the link and paste below
              </Typography>
              <TextField
                id="outlined-basic"
                label="Apple iCal address"
                variant="outlined"
                fullWidth
              />
              <Typography>
                Give your calander a name to identify it with.
              </Typography>
              <TextField
                id="outlined-basic"
                label="Calander Name"
                variant="outlined"
                fullWidth
              />
              <Button
                variant="contained"
                color="primary"
                style={{ marginTop: 10 }}
              >
                Save
              </Button>
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
            <Typography className={classes.heading}>News Feed</Typography>
          </AccordionSummary>
          <AccordionDetails>
            This module will display the headlines from provided news feeds.
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
            <Typography className={classes.heading}>Messages</Typography>
          </AccordionSummary>
          <AccordionDetails>
            This module will display the messages to your screen.
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3a-content"
            id="panel3a-header"
          >
            <Typography className={classes.heading}>
              Spotify Now Playing
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <div>
              This module will display the song currently playing on your
              Spotify account. In order to work properly the module needs read
              access to some of your Spotifiy information. Namely these
              information are:{" "}
              <ul>
                <li>your currently playing track</li>
                <li>your playback status</li>
              </ul>
              Click "I Agree" to accept that this information will be used
              <div>
                <Button
                  variant="contained"
                  color="primary"
                  style={{ marginTop: 10 }}
                  onClick={() => setSpotAgree(true)}
                >
                  I Agree
                </Button>
              </div>
              <div></div>
              <p>
                To provide these data you need an app in the{" "}
                <a
                  href="https://beta.developer.spotify.com/dashboard/login"
                  target="_blank"
                  style={{ color: "lightblue" }}
                >
                  development area of Spotify
                </a>
                . This app provides the above mentioned information to your
                magic mirror. This way it is made sure that you and only you
                have access to your Spotify data.
              </p>
              <Typography variant="h4">Step by Step</Typography>
              <br />
              <Typography variant="h5">
                Step 1: Create a new Spotify app
              </Typography>
              <div id="step1-placeholder">
                <p>
                  Visit{" "}
                  <a
                    href="https://beta.developer.spotify.com/dashboard/login"
                    target="_blank"
                    style={{ color: "lightblue" }}
                  >
                    Spotify for Developers
                  </a>{" "}
                  and create your new app via the button on the top right. It
                  doesn’t matter what you type in the fields since only you will
                  see the app. Maybe you should use something you’ll recognize
                  in the future. When asked whether you are developing a
                  commercial integration click “No.” and accept all the
                  checkmarks in the last step.
                </p>

                <p>
                  After successfully creating the app you’ll land on the app’s
                  dashboard. There you can see your <code>Client ID</code> and
                  your <code>Client Secret</code> (after clicking on “SHOW
                  CLIENT SECRET”). Please <strong>note both information</strong>{" "}
                  for later use.
                </p>

                <p>
                  Click on “Edit Settings” and add{" "}
                  <code> http://HOSTNAME:8888/callback </code> to the Redirect
                  URIs and save your settings.
                </p>
              </div>
              <Typography variant="h5">
                Step 2: Authorize your new app with your Spotify account
              </Typography>
              <p>
                Now you’ll need to give access to your currently playing track
                by filling out the following details. Then Click the save button
                which will bring you to a sign in page to authorise the mirror
                to access your spotify profile
              </p>
              <Container style={{ marginTop: 20 }}>
                <TextField
                  required
                  id="standard-required"
                  label="Client Id"
                  type="password"
                  variant="outlined"
                  fullWidth
                  onChange={(e) => setSpotifyClientId(e.target.value)}
                />
              </Container>
              <Container style={{ marginTop: 20 }}>
                <TextField
                  required
                  id="standard-required"
                  label="Client Secret"
                  type="password"
                  variant="outlined"
                  fullWidth
                  onChange={(e) => setSpotifyClientSecret(e.target.value)}
                />
              </Container>
              <br />
              <a
                onClick={() =>
                  addCreds({
                    creds: [
                      {
                        cred_num: 6,
                        cred_value: spotifyClientId,
                      },
                      {
                        cred_num: 7,
                        cred_value: spotifyClientSecret,
                      },
                    ],
                  })
                }
                href="http://localhost:8888/"
                target="_blank"
                style={{ color: "lightblue" }}
              >
                <Button variant="contained" color="primary" onClick={() => {}}>
                  Save
                </Button>
              </a>
              <div id="login">
                <div id="form-placeholder"></div>
              </div>
              <div>
                <Typography variant="h5">
                  Step 3: Paste AccordionDetails
                </Typography>
              </div>
            </div>
          </AccordionDetails>
        </Accordion>
      </div>
    </Container>
  );
}
