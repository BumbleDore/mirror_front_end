var fs = require("fs");

let cities;
let filteredCities;

fs.readFile("city.list.json", function (err, data) {
  if (err) {
    return console.error(err);
  }
  cities = JSON.parse(data);
  console.log(cities.length);
  filteredCities = cities.filter(function (city) {
    if (city.country === "GB" || city.country === "IE") {
      return city;
    }
  });
  console.log(cities.length);
  console.log(filteredCities.length);

  fs.writeFile(
    "citiesFilter.list.json",
    JSON.stringify(filteredCities),
    function (err) {
      if (err) {
        return console.error(err);
      }
    }
  );
});
