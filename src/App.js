import React from "react";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "./componets/AppBar";
import HomePage from "./pages/HomePage";
import SetupPage from "./pages/SetupPage";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";

function App() {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          type: prefersDarkMode ? "dark" : "light",
        },
      }),
    [prefersDarkMode]
  );

  return (
    <Router>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar />
        <Route exact path="/" component={HomePage} />
        <Route exact path="/setup" component={SetupPage} />
      </ThemeProvider>
    </Router>
  );
}

export default App;
