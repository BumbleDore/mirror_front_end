import React from "react";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Slider from "@material-ui/core/Slider";
import Grid from "@material-ui/core/Grid";
import BrightnessHighIcon from "@material-ui/icons/BrightnessHigh";
import BrightnessLowIcon from "@material-ui/icons/BrightnessLow";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    width: "80%",
    padding: theme.spacing(2, 4, 3),
  },
}));

function valuetext(value) {
  return `${value}`;
}

export default function BrightnessSlider({
  showBrightness,
  closeBrightnessSlider,
}) {
  const classes = useStyles();
  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={showBrightness}
        onClose={closeBrightnessSlider}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={showBrightness}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Brightness</h2>
            <Grid container spacing={2}>
              <Grid item>
                <BrightnessLowIcon />
              </Grid>
              <Grid item xs>
                <Slider
                  defaultValue={30}
                  getAriaValueText={valuetext}
                  aria-labelledby="discrete-slider"
                  valueLabelDisplay="auto"
                  onChange={(event, value) =>
                    axios.get(`http://raspberrypi:8080/api/brightness/${value}`)
                  }
                  min={0}
                  max={100}
                  step={10}
                  marks
                />
              </Grid>
              <Grid item>
                <BrightnessHighIcon />
              </Grid>
            </Grid>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
