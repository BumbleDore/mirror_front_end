import axios from "axios";

export const addCreds = async (creds) => {
  const res = axios.post("https://raspberrypi:3002/add_cred", creds);
  console.log(res);
};
